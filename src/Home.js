import { useState } from 'react';
import BlogList from './BlogList';
const Home = () => {

    const [blogs, setBlogs] = useState([
        { title: 'title one', body: 'body 1', author: 'Maruf', id: '1' },
        { title: 'title two', body: 'body 2', author: 'Maruf', id: '2' },
        { title: 'title three', body: 'body 3', author: 'Maruf', id: '3' }
    ]);
    const handleDelete =(id)=>{
        console.log('Delete this id ',id);
       
    }
    return (
        <ul>
            {/* {blogs.map((blog) => (
                <li> {blog.title} </li>
                <li>{blog.body}</li>
            )

            )} */}
            <BlogList blogs={blogs} handleDelete={handleDelete}/>
        </ul>
    )


}
export default Home;