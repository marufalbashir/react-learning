// import logo from './logo.svg';
//import reactDom from 'react-dom';
import './App.css';
import Navbar from './Navbar';
import Home from './Home';
function App() {
  const title ='Hello This is a title';
  const theMath = 1250*87854;
  return (
    <div className="App">
      <Navbar/>
      <h1>{title}</h1>
      <p>The math output is : {theMath}</p>

      <Home/>
    </div>
  );
}

export default App;
