const BlogList =(props) =>{
    const blogs = props.blogs;
    const handleDelete = props.handleDelete;
    console.log(props);
    return (
        <div className="blog-list">
            {blogs.map(({title, body, id}) => (
                <div className = 'blog-list' key={id}>
                    <h3>{title}</h3>
                    <p>{body}</p>
                    <button onClick={()=> handleDelete(id)} value={id}>Delete</button>
                </div>
            ))}
        </div>
    );
}

export default BlogList;

